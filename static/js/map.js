const apiKey ='pk.eyJ1IjoiYWhtYWRuZWdtIiwiYSI6ImNrdzB4djJsMDBvY2cydXJvdGp3aGdjd3AifQ.GC-5nZzwLkF1g8ZkK_n33Q';
const mymap = L.map('map').setView([26.604414160339953, 29.82816068741451], 5);
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: apiKey
}).addTo(mymap);
// adding marker
let template = `
<h4>Our Branch</h4>
<div style="text-align:center;">
<img src="/assets/img/SKY LOGO.png" height="50" width="50">
</div>
`
const marker = L.marker([23.573508109896665, 45.56834534065265]).addTo(mymap);marker.bindPopup(template);
const newLocal = L.marker([31.874511141514276, -6.3288944199650405]).addTo(mymap);newLocal.bindPopup(template);
const newLocal_1 = L.marker([26.977508645091827, 17.649015742742538]).addTo(mymap);newLocal_1.bindPopup(template);
const newLocal_2 = L.marker([16.352318900885468, 30.39524265922008]).addTo(mymap);newLocal_2.bindPopup(template);
const newLocal_3 = L.marker([34.15727262296096, 35.910706984037084]).addTo(mymap);newLocal_3.bindPopup(template);
const newLocal_4 = L.marker([38.95028233367323, 35.500480995241155]).addTo(mymap);newLocal_4.bindPopup(template);
const newLocal_5 = L.marker([34.99257772036711, 38.52805215020543]).addTo(mymap);newLocal_5.bindPopup(template);
const newLocal_6 = L.marker([15.810589397310842, 47.581382044951745]).addTo(mymap);newLocal_6.bindPopup(template);
const newLocal_7 = L.marker([-21.044321296648988, 46.02098378883079]).addTo(mymap);newLocal_7.bindPopup(template);
const newLocal_8 = L.marker([-1.018365516342339, 15.233362650149406]).addTo(mymap);newLocal_8.bindPopup(template);
const newLocal_9 = L.marker([8.622194918909774, 39.672811961102404]).addTo(mymap);newLocal_9.bindPopup(template);
const newLocal_10 = L.marker([42.16892616980635, 43.494549766906445]).addTo(mymap);newLocal_10.bindPopup(template);
const newLocal_11 = L.marker([26.03449771525668, 50.554582716061]).addTo(mymap);newLocal_11.bindPopup(template);
const newLocal_12 = L.marker([20.054076082921497, -10.1059291838834]).addTo(mymap);newLocal_12.bindPopup(template);
const newLocal_13 = L.marker([-13.400731491664846, 27.533803963502017]).addTo(mymap);newLocal_13.bindPopup(template);
const newLocal_14 = L.marker([52.89734274274465, 18.64987436863481]).addTo(mymap);newLocal_14.bindPopup(template);
const newLocal_15 = L.marker([29.313077758027717, 47.40255749342547]).addTo(mymap);newLocal_15.bindPopup(template);
const newLocal_16 = L.marker([32.99292424736066, 42.95859931675152]).addTo(mymap);newLocal_16.bindPopup(template);
const newLocal_17 = L.marker([-6.462529797012429, 34.77854326004636]).addTo(mymap);newLocal_17.bindPopup(template);
const newLocal_18 = L.marker([62.9054526593635, 16.967378583081004]).addTo(mymap);newLocal_18.bindPopup(template);
const newLocal_19 = L.marker([11.734801075916709, 42.61623436252695]).addTo(mymap);newLocal_19.bindPopup(template);
const newLocal_20 = L.marker([20.627747025906416, 56.140258568229825]).addTo(mymap);newLocal_20.bindPopup(template);
const newLocal_21 = L.marker([28.11083672380876, 2.4978108885017716]).addTo(mymap);newLocal_21.bindPopup(template);
const newLocal_22 = L.marker([0.5496071572790037, 37.96037142507553]).addTo(mymap);newLocal_22.bindPopup(template);
const newLocal_23 = L.marker([2.7324275683957433, 45.19160271651251]).addTo(mymap);newLocal_23.bindPopup(template);
const newLocal_24 = L.marker([-3.312594517035359, 29.919608226578017]).addTo(mymap);newLocal_24.bindPopup(template);
const newLocal_25 = L.marker([31.269789605870596, 36.80181925550943]).addTo(mymap);newLocal_25.bindPopup(template);
const newLocal_26 = L.marker([-1.9929727855759887, 29.91324175048991]).addTo(mymap);newLocal_26.bindPopup(template);
const newLocal_27 = L.marker([45.78988366986499, 25.117503686520433]).addTo(mymap);newLocal_27.bindPopup(template);
const newLocal_28 = L.marker([47.02639416177201, 19.544702786802468]).addTo(mymap);newLocal_28.bindPopup(template);
const newLocal_29 = L.marker([30.96913294095294, 34.83957047927854]).addTo(mymap);newLocal_29.bindPopup(template);
const newLocal_30 = L.marker([17.399390004748536, 9.320333682946575]).addTo(mymap);newLocal_30.bindPopup(template);
