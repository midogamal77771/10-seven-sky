jQuery(document).ready(function($){
    $('.banner-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.thumbnail-slider'
      });
      $('.thumbnail-slider').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.banner-slider',
        dots: false,
        arrows: false,
        centerMode: true,
        focusOnSelect: true
      });
});