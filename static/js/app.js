$(window).on("scroll", function () {
    AOS.init();
});
AOS.init();

// animation in website
const toggleButton = document.getElementsByClassName('toggle-button')[0]
const navbarLinks = document.getElementsByClassName('navbarr-links')[0]
const navbarBrand = document.getElementsByClassName('brand')[0]
toggleButton.addEventListener('click', () => {
    navbarLinks.classList.toggle('navActive')
})
// end of animation


// navbar in small screen
navbarLinks.addEventListener('click', () => {
    navbarLinks.classList.toggle('navActive')
})

navbarBrand.addEventListener('click', () => {
    navbarLinks.classList.remove('navActive')
})

// slider in website
$(document).ready(function(){
    $(".owl-carousel").owlCarousel({
        loop:true,
        margin:10,
        rows:2,
        responsiveClass:true,
        responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
    });
  });
//  end of owl slider

// slick carousel





// if you want to stop auto slide in bootstrap slider 
//document ready
// $(document).ready(function(){
//     //Event for pushed the video
//     $('#carouselExampleFade').carousel({
//         pause: true,
//         interval: false
//     });
// });


var header = document.getElementById("myDIV");
var btns = header.getElementsByClassName("navLink");
for (var i = 0; i < btns.length; i++) {
  btns[i].addEventListener("click", function() {
  var current = document.getElementsByClassName("active");
  current[0].className = current[0].className.replace(" active", "");
  this.className += " active";
  });
};
window.addEventListener("scroll", function(){
    var nav = document.querySelector("nav");
    nav.classList.toggle("white-nv", window.scrollY > 10);
    nav.classList.toggle("navbarr", window.scrollY < 10);
})