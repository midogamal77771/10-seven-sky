from django.db import models


class Product(models.Model):
    name = models.CharField(max_length=200, db_index=True, unique=True)
    short_description = models.CharField(max_length=250, null=True)
    long_description = models.TextField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class ProductImage(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='images')
    image = models.ImageField(upload_to='product/')
