from django.views.generic import ListView
from .models import Product


class ProductsView(ListView):
    model = Product
    template_name = 'products/products.html'
    context_object_name = 'products'
