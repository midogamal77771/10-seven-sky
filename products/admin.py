from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Product, ProductImage


class ProductImageAdminInline(admin.TabularInline):
    model = ProductImage
    min_num = 3
    extra = 5
    readonly_fields = ('show_image', )

    def show_image(self, obj):
        return mark_safe(f"<a href='{obj.image.url}' ><img src='{obj.image.url}' width={200} height={150}></a>")


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'created', 'updated')
    inlines = [ProductImageAdminInline, ]


admin.site.register(Product, ProductAdmin)
