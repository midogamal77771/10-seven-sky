from django.urls import path
from .views import ProductsView


app_name = 'products'

urlpatterns = [
    path('all/', ProductsView.as_view(), name='all'),
]
