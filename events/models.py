from django.db import models
from django.core.validators import FileExtensionValidator


VideoValidator = FileExtensionValidator(allowed_extensions=['MOV', 'avi', 'mp4', 'webm', 'mkv'])


class Event(models.Model):
    title = models.CharField(max_length=500)
    address = models.CharField(max_length=450)
    date = models.DateTimeField()
    timestamp = models.DateTimeField(auto_now_add=True, null=True)
    description = models.TextField()

    def __str__(self):
        return self.title


class ImageEvent(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='images')
    name = models.CharField(max_length=150)
    image = models.ImageField()


class VideoEvent(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name='files')
    title = models.CharField(max_length=150)
    video = models.FileField(validators=[VideoValidator, ])
