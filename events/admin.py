from django.contrib import admin

from .models import Event, ImageEvent, VideoEvent


class ImageEventAdminInline(admin.TabularInline):
    model = ImageEvent
    extra = 3
    min = 1


class VideoEventAdminInline(admin.TabularInline):
    model = VideoEvent
    extra = 3
    min = 0


class EventAdmin(admin.ModelAdmin):
    list_display = ('title', 'address', 'date')
    ordering = ('-date', )
    inlines = [ImageEventAdminInline, ]


admin.site.register(Event, EventAdmin)
