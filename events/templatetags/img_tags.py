from django import template
from random import choice

register = template.Library()


@register.filter(name='rand_img')
def img_refactor(args):
    imgs = [arg.image.url for arg in args if arg and arg.image]
    return choice(imgs) if imgs else None
