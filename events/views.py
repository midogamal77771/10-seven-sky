from django.views.generic import ListView

from .models import Event


class EventView(ListView):
    model = Event
    template_name = 'events/events.html'
    context_object_name = 'events'
