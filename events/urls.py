from django.urls import path

from .views import EventView


app_name = 'events'

urlpatterns = [
    path('all/', EventView.as_view(), name='all')
]
