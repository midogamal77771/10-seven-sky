from django import forms

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Row, Submit

from .models import ContactUsMessage


class ContactUsMessageForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Field('full_name', css_class='form-control'),
            Field('business_name', css_class='form-control'),
            Field('email', css_class='form-control'),
            Field('phone', css_class='form-control'),
            Row(
                Field('country', css_class='form-control'),
                Field('city', css_class='form-control'),
            ),
            Field('subject', css_class='form-control'),
            Field('message', css_class='form-control'),
            Submit(name='Send', value='Send'),
            #    background-color: #948e65;
        )

    class Meta:
        model = ContactUsMessage
        fields = '__all__'
        exclude = ('timestamp', )
