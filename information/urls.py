from django.urls import path

from .views import ContactUsMessageView, AboutUsView, QualityStandardView


app_name = 'information'

urlpatterns = [
    path('about-us/', AboutUsView.as_view(), name='about_us'),
    path('contact-us/', ContactUsMessageView.as_view(), name='contact_us'),
    path('quality-standards/', QualityStandardView.as_view(), name='quality_standard'),
]
