from django import template


register = template.Library()


@register.filter(name='reminder')
def num_reminder(arg):
    if str(arg).isnumeric():
        return int(arg) % 2
    return None

