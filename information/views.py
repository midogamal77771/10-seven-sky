from django.urls import reverse_lazy
from django.utils.translation import gettext as _
from django.views.generic import CreateView, TemplateView
from django.contrib.messages.views import SuccessMessageMixin

from .models import ContactUsMessage, Contact, ChairmanWord, Company, Vision, Mission, QualityStandard, Certificate
from .forms import ContactUsMessageForm


class ContactUsMessageView(CreateView, SuccessMessageMixin):
    model = ContactUsMessage
    form_class = ContactUsMessageForm
    template_name = 'information/contact_us.html'
    success_message = _('Message has been saved')
    success_url = reverse_lazy('home')
    extra_context = {
        'contact_info': Contact.objects.first()
    }


class AboutUsView(TemplateView):
    template_name = 'information/about_us.html'
    extra_context = {
        'chairman_word': ChairmanWord.objects.first(),
        'companies': Company.objects.all(),
        'visions': Vision.objects.all(),
        'missions': Mission.objects.all(),
    }


class QualityStandardView(TemplateView):
    template_name = 'information/quality_standards.html'
    extra_context = {
        'quality_standard': QualityStandard.objects.first(),
        'certificates': Certificate.objects.all()
    }
