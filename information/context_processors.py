from .models import Contact


def main_info(request):
    contact = Contact.objects.first()
    return {
        'main_info': {
            'twitter': contact.twitter,
            'youtube': contact.youtube,
            'linkedin': contact.linkedin,
            'facebook': contact.facebook,
            'instagram': contact.instagram,
            'whatsapp': contact.whatsapp
        }
    } if contact else {}
