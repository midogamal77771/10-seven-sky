from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import (Company, Catalogue, Contact, ContactUsMessage, QualityStandard, Certificate, ChairmanWord, Vision,
                     Mission)


class NOAddDeletePermission(admin.ModelAdmin):
    max_add: int = 1

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request):
        if self.model.objects.count() >= self.max_add:
            return False
        return super().has_add_permission(request)


class ContactAdmin(NOAddDeletePermission):
    list_display = ('email', 'phone', 'whatsapp', 'show_links')
    readonly_fields = ('show_links', )

    def show_links(self, instance):
        return mark_safe(f"""
            <a href="{instance.facebook}" class="px-2"><i class="fab fa-facebook"></i></a>
            <a href="{instance.instagram}" class="px-2"><i class="fab fa-instagram"></i></a>
            <a href="{instance.twitter}" class="px-2"><i class="fab fa-twitter"></i></a>
            <a href="{instance.linkedin}" class="px-2"><i class="fab fa-linkedin"></i></a>
            <a href="{instance.youtube}" class="px-2"><i class="fab fa-youtube"></i></a>
            <a href="https://wa.me/+{instance.whatsapp}" class="px-2"><i class="fab fa-whatsapp"></i></a>
        """) if instance else "---"

    show_links.short_description = 'Links'


class ChairmanWordAdmin(NOAddDeletePermission):
    list_display = ('title', )


class QualityStandardAdmin(NOAddDeletePermission):
    list_display = ('title', )


class CatalogueAdmin(NOAddDeletePermission):
    list_display = ('__str__', )


class ContactUsMessageAdmin(NOAddDeletePermission):
    list_display = ('full_name', 'business_name', 'email', 'phone', 'subject', 'timestamp')
    ordering = ('-timestamp', )


admin.site.register(Vision)
admin.site.register(Mission)
admin.site.register(Company)
admin.site.register(Certificate)
admin.site.register(Contact, ContactAdmin)
admin.site.register(Catalogue, CatalogueAdmin)
admin.site.register(ChairmanWord, ChairmanWordAdmin)
admin.site.register(QualityStandard, QualityStandardAdmin)
admin.site.register(ContactUsMessage, ContactUsMessageAdmin)
