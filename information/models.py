from django.db import models
from django.utils.translation import gettext as _
from django.core.validators import RegexValidator, FileExtensionValidator


PDFValidator = FileExtensionValidator(allowed_extensions=['pdf', ])
PhoneNumberValidator = RegexValidator(r'^[0-9]{12,14}$', _('You entre invalid number'))


class Company(models.Model):
    name = models.CharField(max_length=250)
    logo = models.ImageField(upload_to='company_logo/')
    short_description = models.TextField()
    long_description = models.TextField()

    def __str__(self):
        return self.name


class Catalogue(models.Model):
    file = models.FileField(null=True, validators=[PDFValidator], upload_to='catalogue/file/')


class Contact(models.Model):
    address = models.TextField(_('Address'))
    email = models.EmailField(_('Email'))
    phone = models.CharField(_('Phone Number'), max_length=20, validators=[PhoneNumberValidator, ])
    linkedin = models.URLField(_('LinkedIn link'))
    facebook = models.URLField(_('Facebook link'))
    instagram = models.URLField(_('Instagram link'))
    twitter = models.URLField(_('Twitter link'))
    youtube = models.URLField(_('YouTube link'))
    whatsapp = models.CharField(_('Whatsapp number'), max_length=20, validators=[PhoneNumberValidator, ])

    def __str__(self):
        return self.email


class ContactUsMessage(models.Model):
    full_name = models.CharField(max_length=150)
    business_name = models.CharField(max_length=150)
    email = models.EmailField()
    phone = models.CharField(max_length=15, validators=[PhoneNumberValidator, ])
    country = models.CharField(max_length=150)
    city = models.CharField(max_length=150)
    subject = models.CharField(max_length=150)
    message = models.TextField()
    timestamp = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.full_name


class QualityStandard(models.Model):
    title = models.CharField(max_length=150)
    image = models.ImageField(upload_to='quality_standards/')
    description = models.TextField()

    def __str__(self):
        return self.title


class Certificate(models.Model):
    name = models.CharField(max_length=150)
    image = models.ImageField(upload_to='certificate/')

    def __str__(self):
        return self.name


class ChairmanWord(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField()

    def __str__(self):
        return self.title


class Vision(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField()

    def __str__(self):
        return self.title


class Mission(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField()

    def __str__(self):
        return self.title
