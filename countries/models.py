from django.db import models


class Country(models.Model):
    name = models.CharField(max_length=200)
    flag = models.ImageField(upload_to='country_flag/')
    lat = models.DecimalField(decimal_places=4, max_digits=10)
    long = models.DecimalField(decimal_places=4, max_digits=10)

    class Meta:
        verbose_name = 'Countries'
        verbose_name_plural = 'Countries'
