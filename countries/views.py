from django.views.generic import TemplateView


class BusinessView(TemplateView):
    template_name = 'business/business.html'
