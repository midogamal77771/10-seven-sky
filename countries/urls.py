from django.urls import path
from .views import BusinessView

app_name = 'country'

urlpatterns = [
    path('business/', BusinessView.as_view(), name='business',)
]
