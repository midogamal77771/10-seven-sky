from django.views.generic import TemplateView

from events.models import Event
from products.models import Product
from countries.models import Country
from information.models import Company, Catalogue, QualityStandard, Certificate, ChairmanWord, Vision, Mission


class HomePage(TemplateView):
    template_name = 'home.html'
    extra_context = {
        'companies': Company.objects.all(),
        'events': Event.objects.all().order_by('-date')[:2],
        'catalogue': Catalogue.objects.first(),
        'countries': Country.objects.all(),
        'quality_standard': QualityStandard.objects.first(),
        'certificates': Certificate.objects.all(),
        'charmin_words': ChairmanWord.objects.first(),
        'missions': Mission.objects.all(),
        'visions': Vision.objects.all(),
        'products': Product.objects.all(),
    }
