from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

from .views import HomePage


urlpatterns = [
    path('admin/', admin.site.urls),
    path('info/', include('information.urls', namespace='info')),
    path('events/', include('events.urls', namespace='events')),
    path('products/', include('products.urls', namespace='products')),
    path('countries/', include('countries.urls', namespace='countries')),
    path('', HomePage.as_view(), name='home')
]


if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
